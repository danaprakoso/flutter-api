<?php

class User extends CI_Controller {
	
	public function login() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$users = $this->db->query("SELECT * FROM `users` WHERE `email`='" . $email . "'")->result_array();
		if (sizeof($users) > 0) {
			if ($users[0]['password'] == $password) {
				echo json_encode(array('user_id' => $users[0]['id'], 'response_code' => 1));
			} else {
				echo json_encode(array('response_code' => -1));
			}
		} else {
			echo json_encode(array('response_code' => -2));
		}
	}
	
	public function login_with_google() {
		$email = $this->input->post('email');
		$displayName = $this->input->post('display_name');
		$users = $this->db->query("SELECT * FROM `users` WHERE `email`='" . $email . "'")->result_array();
		if (sizeof($users) > 0) {
			echo json_encode(array('user_id' => $users[0]['id'], 'response_code' => 1));
		} else {
			$this->db->insert('users', array(
				'email' => $email,
				'name' => $displayName
			));
			$id = intval($this->db->insert_id());
			echo json_encode(array('user_id' => $id, 'response_code' => 1));
		}
	}
	
	public function login_with_facebook() {
		$email = $this->input->post('email');
		$displayName = $this->input->post('display_name');
		$users = $this->db->query("SELECT * FROM `users` WHERE `email`='" . $email . "'")->result_array();
		if (sizeof($users) > 0) {
			echo json_encode(array('user_id' => $users[0]['id'], 'response_code' => 1));
		} else {
			$this->db->insert('users', array(
				'email' => $email,
				'name' => $displayName
			));
			$id = intval($this->db->insert_id());
			echo json_encode(array('user_id' => $id, 'response_code' => 1));
		}
	}
	
	public function update_user_details() {
		$userID = intval($this->input->post('user_id'));
		$lat = doubleval($this->input->post('latitude'));
		$lng = doubleval($this->input->post('longitude'));
		$phone = $this->input->post('phone');
		$imei = $this->input->post('imei');
		$this->db->query("UPDATE `users` SET `latitude`=" . $lat . ", `longitude`=" . $lng . ", `phone`='" . $phone . "', `imei`='" . $imei . "' WHERE `id`=" . $userID);
	}
}